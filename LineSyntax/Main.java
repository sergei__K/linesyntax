package LineSyntax;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
        public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строковое выражение");
        String string = scanner.nextLine();
        String pattern = "(\\s*\\(*\\w+\\)*|\\\".*\\\"|\\'.*\\')\\s*(\\+\\s*(\\(*\\w+\\)*|\\(*\\'.?\\'\\)*|\\(*\\\".*\\\"\\)*))*|\\(*\\'.?\\'\\)*";
        Pattern p = Pattern.compile(pattern);
        Matcher matcher = p.matcher(string);
        System.out.println(syntax(matcher, string));
    }


    /**
     * Метод для проверки на правельность ввода строкового выражения.
     *
     * @param matcher
     * @param string
     * @return
     */
    private static String syntax(Matcher matcher, String string) {
        String result;
        if (matcher.matches()) {
            if (brackets(string)) {
                result = "всё верно";
            } else {
                result = "Ошибка в скобках.";
            }
        } else {
            result = "Ошибка в синтаксисе введенного выражения.";
        }
        return result;
    }

    /**
     * Метод для проверки количества скобок и сравнения количество открывающих и закрывающих скобок.
     *
     * @param string
     * @return
     */
    private static boolean brackets(String string) {
        int opening=0;
        int close=0;
        char[] array = string.toCharArray();
        for (int i=0;i<array.length;i++){
            if (close<=opening) {
                if (array[i] == '(') {
                    opening++;
                } else if (array[i] == ')') {
                    close++;
                }
            }else{
                return false;
            }
        }
        return opening==close;
    }

}